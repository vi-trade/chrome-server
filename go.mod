module chrome-server

go 1.17

require github.com/zserge/lorca v0.1.10

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20211108170745-6635138e15ea // indirect
)
