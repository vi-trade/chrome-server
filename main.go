package main

import (
	"chrome-server/pkg/chromeserver"
	"chrome-server/pkg/wsocket"
	"flag"
	"fmt"
	"os"
	"os/signal"
)

var port = "4321"
var root = "./www"
var url = "http://localhost:4321/"

func main() {
	fmt.Println("chrome-server v 001")
	port, root, url = readCommandLineParams()

	wsocket.SetupRoutes()
	wsocket.Onmessage = messageHandler
	go chromeserver.OpenBrowser(url)
	chromeserver.SetupRoutes(root)

	// chromeserver.StartServer(port)
	// Starts HTTP server in goroutine
	go chromeserver.StartServer(port)
	// wait for CTRL-C
	waitForBreak()
}

func readCommandLineParams() (port, rootDir, url string) {
	flag.StringVar(&port, "port", "4321", "port number")
	flag.StringVar(&rootDir, "root", "./www", "root directory for HTTP server")
	flag.StringVar(&url, "url", "http://localhost:4321/index.html", "URL to load in the browser")
	flag.Parse()
	flag.Usage()
	return
}

func messageHandler(msgType int, bytes []byte) {
	wsocket.WriteMessage(msgType, bytes)
}

func waitForBreak() {
	signalChannel := make(chan os.Signal)
	signal.Notify(signalChannel, os.Interrupt)
	<-signalChannel
}
