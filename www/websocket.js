var onmessage
var onopen
var onclose
var address 
var socket = null
var timeOut

function close(){
    clearTimeout(timeOut)
    socket.destroy()
    socket = null
}

function open(addr) {
    // console.log('[open]')

    if (addr) {
        address = addr
    }
    clearTimeout(timeOut)


    try {
        socket = new WebSocket(address)
        // console.log(`[open] Socket created: ${address}`)

        socket.onopen = function () {
            clearTimeout(timeOut)
            console.log(`[socket.onopen] Socket opened: ${address}`)
            if (typeof onopen === 'function') onopen()
        }
        socket.onclose = function (event) {
            console.log( `[socket.onclose] Connection died ${address}, code=${event.code}` )
            if (typeof onclose === 'function') onclose(event)
            open()
        }
        socket.onerror = function (error) {
            console.log(`[socket.onerror] `, error)
        }
        socket.onmessage = function (e) {
            console.log(`[socket.onmessage]: ${e.data}`)
            if (typeof onmessage === 'function') onmessage(e.data)
        }
    } catch (err) {
        console.log('[open] Socket creating error:', err)
        timeOut = setTimeout(open, 2000)
    }
}

function send (message) {
    try {
        socket.send(message)
    } catch (error) {
        console.log('[send] Error: ', error)
    }
}

function setOnclose(f) {
    onclose = f
}
function setOnopen(f) {
    onopen = f
}
function setOnmessage(f) {
    onmessage = f
}

export {address, open, close, send, setOnclose, setOnopen, setOnmessage}