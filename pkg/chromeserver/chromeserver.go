package chromeserver

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/zserge/lorca"
)

var URL = "http://localhost:4321/"
var ui lorca.UI

func SetupRoutes(rootDir string) {
	http.Handle("/", http.FileServer(http.Dir(rootDir)))
	http.HandleFunc("/load", loadHandler)
	http.HandleFunc("/eval", evalHandler)
	http.HandleFunc("/open", openHandler)
	http.HandleFunc("/close", closeHandler)
	http.HandleFunc("/exit", exitHandler)
}

func OpenBrowser(url string) {
	URL = url
	if URL == "" {
		fmt.Println("OpenBrowser(): URL is empty")
		return
	}
	fmt.Println("Chrome path=", lorca.ChromeExecutable())
	var err error
	ui, err = lorca.New(URL, "", 800, 500)
	if err != nil {
		log.Fatal(err)
	}
	defer ui.Close()
	<-ui.Done()
	ui = nil
	os.Exit(0)
}

func StartServer(port string) {
	fmt.Printf("\nserver started http://localhost:%s \n\n", port)
	err := http.ListenAndServe("localhost:"+port, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func CloseBrowser() {
	if ui != nil {
		ui.Close()
		ui = nil
	}
}

func openHandler(w http.ResponseWriter, r *http.Request) {
	go OpenBrowser(URL)
}

func closeHandler(w http.ResponseWriter, r *http.Request) {
	CloseBrowser()
}

func exitHandler(w http.ResponseWriter, r *http.Request) {
	CloseBrowser()
	os.Exit(0)
}

func loadHandler(w http.ResponseWriter, r *http.Request) {
	if ui == nil {
		fmt.Fprintln(w, "Chrome window is closed")
		return
	}
	// returns an array of items
	urls, ok := r.URL.Query()["url"]
	if !ok {
		fmt.Fprintln(w, "Param 'url' is missing")
		return
	}
	// we only want the first item.
	passedUrl := string(urls[0])

	if passedUrl == "" {
		fmt.Fprintln(w, "Param 'url' is empty")
		return
	}

	ui.Load(passedUrl)
}

func evalHandler(w http.ResponseWriter, r *http.Request) {
	if ui == nil {
		fmt.Println("evalHandler(): Browser window is not opened.")
		fmt.Fprintln(w, "evalHandler(): Browser window is not opened. /openwindow first")
	} else {
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}
		expression := r.FormValue("expression")
		s := Eval(expression)
		fmt.Println(s)
		fmt.Fprint(w, s)
	}
}

func Eval(expression string) (result string) {
	if ui == nil {
		fmt.Println("Eval(): Browser window is not opened.")
		return
	}
	evalResult := ui.Eval(expression)
	result = (evalResult.String())
	// If evalResult is a number result will be empty.
	// Transform it to a string
	if result == "" {
		result = string(evalResult.Bytes())
		// if string is empty remove double quotes
		if result == "\"\"" {
			result = ""
		}
	}
	return
}
