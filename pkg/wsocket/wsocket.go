package wsocket

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// callback to react to messages
var Onmessage func(int, []byte) = nil
var wsConnection *websocket.Conn = nil

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { /*var origin = r.Header.Get("origin")*/ return true },
}

func WriteMessage(msgType int, msg []byte) {
	if wsConnection == nil {
		fmt.Println("WriteMessage: No connection to the client")
		return
	}
	if err := wsConnection.WriteMessage(msgType, msg); err != nil {
		log.Println("WriteMessage error:", err)
		return
	}

}

func reader(ws *websocket.Conn) {
	for {
		// Read message from browser
		msgType, msg, err := ws.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		// Print the message to the console
		fmt.Printf("%s msgType=%d Sent: %s\n", ws.RemoteAddr(), msgType, string(msg))
		// call Onmessage callback
		if Onmessage != nil {
			Onmessage(msgType, msg)
		}
	}
}

// ServeWs WebSocket endpoint
func ServeWs(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("err=", err)
	}
	wsConnection = ws
	reader(ws)
}

func SetupRoutes() {
	http.HandleFunc("/ws", ServeWs)
}
